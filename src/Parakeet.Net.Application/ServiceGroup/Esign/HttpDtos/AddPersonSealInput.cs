﻿//using WebApiClientCore;

//namespace Parakeet.Net.ServiceGroup.Esign.HttpDtos
//{
//    public class AddPersonSealInput
//    {
//        [AliasAs("accountId")]
//        public string AccountId { get; set; }

//        /// <summary>
//        /// 生成印章的颜色，RED（红色）、BLACK（黑色）、BLUE（蓝色）
//        /// </summary>
//        [AliasAs("color")]
//        public string Color { get; set; }

//        /// <summary>
//        /// 模板类型，SQUARE（正方形），RECTANGLE（长方形），FZKC（艺术字体），YYGXSF（艺术字体），HYLSF（艺术字体），BORDERLESS（无框矩形），HWLS（华文隶书），HWXK（华文行楷），HWXKBORDER（带框华文行楷），YGYJFCS（叶根友疾风草书），YGYMBXS（叶根友行书）
//        /// </summary>
//        [AliasAs("templateType")]
//        public string TemplateType { get; set; }
//    }
//}
