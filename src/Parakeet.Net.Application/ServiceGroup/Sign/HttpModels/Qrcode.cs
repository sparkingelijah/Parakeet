﻿//using WebApiClientCore;

//namespace Parakeet.Net.ServiceGroup.Sign.HttpModels
//{
//    public class QrCode
//    {
//        /// <summary>
//        /// 水印名称
//        /// </summary>
//        [AliasAs("metaname")]
//        public string MetaName { get; set; }

//        /// <summary>
//        /// 二维码信息
//        /// </summary>
//        [AliasAs("textval")]
//        public string TextVal { get; set; }

//        /// <summary>
//        /// 二维码X坐标位置，以文档页面左边为起点向右偏移x位置 (支持像素和百分比设置)
//        /// 例如:x:”50”或者 x:”50%”
//        /// </summary>
//        [AliasAs("x")]
//        public string X { get; set; }

//        /// <summary>
//        /// 二维码Y坐标位置，以文档页面左边为起点向右偏移y位置 (支持像素和百分比设置
//        /// 例如:y:”50”或者y:”50%”
//        /// </summary>
//        [AliasAs("y")]
//        public string Y { get; set; }

//        /// <summary>
//        /// 二维码宽度 ，默认200
//        /// </summary>
//        [AliasAs("width")]
//        public string Width { get; set; }

//        /// <summary>
//        /// 二维码宽度 ，默认200
//        /// </summary>
//        [AliasAs("height")]
//        public string Height { get; set; }
//    }
//}