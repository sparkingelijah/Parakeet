﻿//using WebApiClientCore;

//namespace Parakeet.Net.ServiceGroup.Sign.HttpModels
//{
//    public class WaterMarkImage
//    {
//        /// <summary>
//        /// 水印名称
//        /// </summary>
//        [AliasAs("metaname")]
//        public string MetaName { get; set; }

//        /// <summary>
//        /// 图片水印放置X坐标位置，以文档页面左边为起点向右偏移x位置 (支持像素和百分比设置)
//        /// 例如:x:”50”或者 x:”50%”
//        /// </summary>
//        [AliasAs("x")]
//        public string X { get; set; }

//        /// <summary>
//        /// 图片水印放置Y坐标位置，以文档页面左边为起点向右偏移y位置 (支持像素和百分比设置)
//        /// 例如:y:”50”或者y:”50%”
//        /// </summary>
//        [AliasAs("y")]
//        public string Y { get; set; }

//        /// <summary>
//        /// base64格式的水印图片
//        /// </summary>
//        [AliasAs("imgData")]
//        public string ImgData { get; set; }
//    }
//}