﻿namespace Parakeet.Net.ROClient
{
    public abstract class ModelBase
    {
        public abstract string CommandName { get; }
    }
}