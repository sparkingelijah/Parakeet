﻿namespace Parakeet.Net.ROClient.Models
{
    public class ProjectExtended
    {
        /// <summary>
        /// 键
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// 值1
        /// </summary>
        public string Value1 { get; set; }

        /// <summary>
        /// 值2
        /// </summary>
        public string Value2 { get; set; } = "";

        /// <summary>
        /// 值3
        /// </summary>
        public string Value3 { get; set; } = "";
    }
}