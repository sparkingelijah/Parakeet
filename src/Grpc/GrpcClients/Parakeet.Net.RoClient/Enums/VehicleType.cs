﻿namespace Parakeet.Net.ROClient
{
    public enum VehicleType
    {
        /// <summary>
        /// 轿车
        /// </summary>
        Car = 0,

        /// <summary>
        /// 渣土车
        /// </summary>
        SlagCar = 1,

        /// <summary>
        /// 罐车
        /// </summary>
        TankCar = 2,

        /// <summary>
        /// 运料车
        /// </summary>
        Truck = 3,

        /// <summary>
        /// 混凝土搅拌车
        /// </summary>
        MixerTruck = 4,

        /// <summary>
        /// 其他
        /// </summary>
        Other = 9
    }
}
