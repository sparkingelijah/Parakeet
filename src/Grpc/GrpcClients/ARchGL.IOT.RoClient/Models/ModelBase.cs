﻿namespace ARchGL.IOT.RoClient
{
    public abstract class ModelBase
    {
        public abstract string CommandName { get; }
    }
}