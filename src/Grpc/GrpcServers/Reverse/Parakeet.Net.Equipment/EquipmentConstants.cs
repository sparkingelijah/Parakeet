﻿namespace Parakeet.Net.Equipment
{
    public class EquipmentConstants
    {
        public const string HUNAN = "430000";
        public const string CHONGQING = "500000";
        public const string SICHUAN = "510000";
        public const string STANDARD = "990000";

        public const string JOX = "0063";
        public const string TYTEST = "0070";
        public const string UFACE = "0071";
        public const string COMMON = "0000";//默认供应商专用
        public const string REGISTER = "0000";//注册专用
        public const string VISOINZENITH = "0072"; //臻视科技


        public const int GATE = 1005;
        public const int EQUIPMENT = 1000;

        public const string EQUIPMENT_EXCHANGE = "exchange.direct.equipment";
        public const string GATE_EXCHANGE = "exchange.direct.gate";

        public const string REVERSE_HANDLER_TYPE = "reverse";
        public const string FEEDBACK_HANDLER_TYPE = "feedback";
        public const string REGISTER_HANDLER_TYPE = "register";

        public const string REGISTER_PERSON_COMMAND = "register_person";
        public const string UPDATE_PERSON_COMMAND = "update_person";
        public const string ADD_PERSON_FACE_COMMAND = "add_person_face";
        public const string UPDATE_PERSON_FACE_COMMAND = "update_person_face";
        public const string DELETE_PERSON_COMMAND = "delete_person";
        public const string FP_TERMINAL_SETTING_COMMAND = "terminal_setting";
        public const string FP_TERMINAL_BASIC_COMMAND = "terminal_basic";
    }
}
