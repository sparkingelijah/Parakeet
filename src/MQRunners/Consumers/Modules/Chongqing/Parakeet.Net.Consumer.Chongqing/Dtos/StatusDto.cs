﻿namespace Parakeet.Net.Consumer.Chongqing.Dtos
{
    public class StatusDto
    {
        /// <summary>
        /// Code
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// text
        /// </summary>
        public string Text { get; set; }
    }
}
