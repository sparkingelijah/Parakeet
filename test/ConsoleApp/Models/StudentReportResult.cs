﻿namespace ConsoleApp.Models
{
    public class StudentReportResult
    {
        public string Name { get; set; }
        //public string SubjectName { get; set; } 
        //public string Score { get; set; } 

        public string SubjectScore { get; set; }
    }
}
